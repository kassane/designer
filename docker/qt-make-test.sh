#!/bin/sh

echo
qmake --version
echo
g++ -v
echo

DIR=`mktemp -d`
cd $DIR

qmake ..
make
