#ifndef EQUALIZER_HPP
#define EQUALIZER_HPP
#pragma once
#include <QPixmap>
#include "draganddrop.hpp"

namespace {
#define GETTER(_TYPE, _FUNCTION, _VAR) \
    _TYPE _FUNCTION() {                \
        return _VAR;                   \
    }
#define SETTER(_TYPE, _FUNCTION, _VAR)  \
    void _FUNCTION(const _TYPE &_new) { \
        _VAR = _new;                    \
    }

#define PROPERTY(_TYPE, _FUNCTION, _VAR) \
    GETTER(_TYPE, _FUNCTION, _VAR)       \
    SETTER(_TYPE, _FUNCTION, _VAR)
}  // namespace

class Equalizer : public DragAndDrop {
    Q_OBJECT

   public:
    Equalizer(QWidget *parent = Q_NULLPTR);
    PROPERTY(QPixmap, EqualizerPNG, _equalizer)

   private:
    QPixmap _equalizer;
};

#endif  // EQUALIZER_HPP
