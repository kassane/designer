#include "mydesigner.hpp"
#include <QApplication>

int main(int argc, char *argv[])
{
    //
#ifdef Q_OS_WIN
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif


    QApplication a(argc, argv);
    MyDesigner w;
    //a.setStyle("fusion");
    //w.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );
    w.setMinimumSize(QSize(1024,720));
    //w.setStyleSheet("QWidget{background-color:#1d1d1d;color: #ffffff;}");
    w.show();

    return a.exec();
}
