#include "ciuoptions.hpp"
#include "equalizer.hpp"
#include "button.hpp"
#include "inifiles.hpp"

Autorun::Autorun(QWidget *parent) : DragAndDrop(parent) {
    setFrameStyle(QFrame::Sunken | QFrame::NoFrame);
    setAcceptDrops(true);
    auto ini = new IniFiles(this);
    QScopedPointer<Equalizer> mEq(new Equalizer(this));
    auto eq       = new QLabel(this);
    auto bt       = new QLabel(this);
    auto btU      = new QLabel(this);
    auto btn      = new Button(this);
    auto btnUnins = new Button(this);

    btn->ButtonPNG(QPixmap("Setup/Autorun/AButton.png"));
    btnUnins->ButtonPNG(QPixmap("Setup/Autorun/AButton.png"));
    mEq->EqualizerPNG(QPixmap("Setup/Autorun/EqualizerAR.png"));
    eq->setPixmap(mEq->EqualizerPNG());
    eq->setScaledContents(true);
    eq->move(300, 350);
    bt->setPixmap(btn->ButtonPNG());
    btU->setPixmap(btnUnins->ButtonPNG());
    btU->move(129, 240);
    bt->setScaledContents(true);
    btU->setScaledContents(true);
}

Autorun::~Autorun() {
    // ini->deleteLater();
}

QPixmap Autorun::CustomInstallerImage() const {
    return QPixmap("Setup/Autorun/Autorun.png");
}

Installer::Installer(QWidget *parent) : DragAndDrop(parent) {
    setFrameStyle(QFrame::Sunken | QFrame::NoFrame);
    setAcceptDrops(true);

    QScopedPointer<Equalizer> mEq(new Equalizer(this));
    auto                  eq = new QLabel(this);
    auto btn      = new Button(this);
    auto bt       = new QLabel(this);

    btn->ButtonPNG(QPixmap("Setup/Setup/Button.png"));
    mEq->EqualizerPNG(QPixmap("Setup/Setup/Equalizer.png"));
    eq->setPixmap(mEq->EqualizerPNG());
    eq->move(100, 200);
    bt->setPixmap(btn->ButtonPNG());
    eq->setScaledContents(true);
    bt->setScaledContents(true);
}

Installer::~Installer() {
    // ini->deleteLater();
}

QPixmap Installer::CustomInstallerImage() const {
    return QPixmap("Setup/Setup/Installer1.png");
}

SmallInstaller::SmallInstaller(QWidget *parent) : DragAndDrop(parent) {
    setMinimumSize(200, 200);
    setFrameStyle(QFrame::Sunken | QFrame::NoFrame);
    setAcceptDrops(true);

    QScopedPointer<Button> musicBtn(new Button(this));
    QLabel *               music = new QLabel(this), *cancel = new QLabel(this);
    musicBtn->ButtonPNG(QPixmap("Setup/Setup/SmallButton.png"));
    music->setPixmap(musicBtn->ButtonPNG());
    cancel->setPixmap(musicBtn->ButtonPNG());
    cancel->setScaledContents(true);
    music->setScaledContents(true);
    cancel->move(260, 215);
    music->move(50, 215);
}

SmallInstaller::~SmallInstaller() {
    // ini->deleteLater();
}

QPixmap SmallInstaller::CustomInstallerImage() const {
    return QPixmap("Setup/Setup/SmallInstaller.png");
}

QPixmap SmallInstaller::BackgroundImage() const {
    return QPixmap("Setup/Background/1.jpg");
}
