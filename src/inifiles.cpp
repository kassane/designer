#include "inifiles.hpp"
#include <QDebug>

IniFiles::IniFiles(QObject *parent) : QObject(parent)
{
    m_sSettingsFile = "Setup/"+m_filename;

//    if (m_pButton)
//     {
//      connect(m_pButton, SIGNAL (released()),this, SLOT (handleButton()));
    //     }
}

IniFiles::~IniFiles()
{
    //Save();
}

void IniFiles::Load()
{
    QSettings settings(m_sSettingsFile, QSettings::IniFormat);
     //QString sText = settings.value(m_pEdit, m_value).toString();
     settings.beginGroup(m_group);
//     if (m_pLabel)
//     {
//      m_pLabel->setText(sText);
//     }
     const QStringList childkeys = settings.allKeys();
     foreach(const QString& value, childkeys)
     {
         Myvalues << settings.value(value).toString();
     }
         /*
          * Modelo INI file:
          * [Group]
          * key=value
         */
         //m_value = Myvalues.value;
     for(int i=0;i<childkeys.size();++i)
     {
         if(m_pEdit == childkeys[i] && m_value != Myvalues[i])
             m_value = Myvalues[i];
         qDebug() << "Opening: " << m_filename << "Read: " << childkeys[i] << "=" << Myvalues[i];
     }
     settings.endGroup();
}

void IniFiles::Save()
{
    QSettings settings(m_sSettingsFile, QSettings::IniFormat);
     //QString sText = m_pEdit.isEmpty() ? m_pEdit : "Null";
     settings.beginGroup(m_group);
     settings.setValue(m_pEdit, m_value);
     //qDebug() << "Saved: " << m_pEdit << " and value:" << m_value;
     settings.endGroup();
}

void IniFiles::Edit(const QString& group, const QString& value, const QString& category)
{
    m_group = group;
    m_value = value;
    m_pEdit = category;
}

void IniFiles::setfilename(const QString &filename)
{
    m_filename = filename;
}

void IniFiles::setgroup(const QString &gname)
{
    m_group = gname;
}

void IniFiles::setCategory(const QString &catName)
{
    m_pEdit = catName;
}

QString IniFiles::filename() const
{
    return m_filename;
}

QString IniFiles::groupName() const
{
    return m_group;
}
