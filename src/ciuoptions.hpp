#ifndef AUTORUN_HPP
#define AUTORUN_HPP

#include "draganddrop.hpp"

class Autorun : public DragAndDrop {
    Q_OBJECT
   public:
    Autorun(QWidget *parent = Q_NULLPTR);
    ~Autorun();
    QPixmap CustomInstallerImage() const;
};

class Installer : public DragAndDrop {
    Q_OBJECT
   public:
    Installer(QWidget *parent = Q_NULLPTR);
    ~Installer();
    QPixmap CustomInstallerImage() const;
};

class SmallInstaller : public DragAndDrop {
    Q_OBJECT
   public:
    SmallInstaller(QWidget *parent = Q_NULLPTR);
    ~SmallInstaller();
    QPixmap CustomInstallerImage() const;
    QPixmap BackgroundImage() const;
};

#endif  // AUTORUN_HPP
