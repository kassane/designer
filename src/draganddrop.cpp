#include "draganddrop.hpp"
#include <QDebug>

DragAndDrop::DragAndDrop(QWidget *parent) : QFrame(parent) {
    //
}
//=======================DragandDrop=============================================================
void DragAndDrop::dragEnterEvent(QDragEnterEvent *event) {
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}
//-------------------------------------------------------------------------------------------------
void DragAndDrop::dragMoveEvent(QDragMoveEvent *event) {
    emit PosX(event->pos().x());
    emit PosY(event->pos().y());

    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}
//-------------------------------------------------------------------------------------------------
void DragAndDrop::dropEvent(QDropEvent *event) {
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        QByteArray itemData =
            event->mimeData()->data("application/x-dnditemdata");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        QPixmap pixmap;
        QPoint  offset;
        dataStream >> pixmap >> offset;

        QLabel *newIcon = new QLabel(this);
        newIcon->setPixmap(pixmap);
        newIcon->move(event->pos() - offset);
        newIcon->show();
        newIcon->setAttribute(Qt::WA_DeleteOnClose);

        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}
//-------------------------------------------------------------------------------------------------
void DragAndDrop::mousePressEvent(QMouseEvent *event) {
    qDebug() << "Mouse Press!";
    QLabel *child = static_cast<QLabel *>(childAt(event->pos()));
    if (!child) return;

    QPixmap pixmap = *child->pixmap();

    QByteArray  itemData;
    QDataStream dataStream(&itemData, QIODevice::WriteOnly);
    dataStream << pixmap << QPoint(event->pos() - child->pos());

    QMimeData *mimeData = new QMimeData;
    mimeData->setData("application/x-dnditemdata", itemData);

    QDrag *drag = new QDrag(this);
    drag->setMimeData(mimeData);
    drag->setPixmap(pixmap);
    drag->setHotSpot(event->pos() - child->pos());

    QPixmap  tempPixmap = pixmap;
    QPainter painter;
    painter.begin(&tempPixmap);
    painter.fillRect(pixmap.rect(), QColor(127, 127, 127, 127));
    painter.end();

    child->setPixmap(tempPixmap);

    if (drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction) ==
        Qt::MoveAction) {
        child->close();
    } else {
        child->show();
        child->setPixmap(pixmap);
    }
    // QWidget::mousePressEvent(event);
}
//-----------------------------------------------------------------------------------------------
void DragAndDrop::mouseMoveEvent(QMouseEvent *event) {
    qDebug() << "Mouse Move!";
    Q_UNUSED(event)
    // QWidget::mouseMoveEvent(event);
}
//-----------------------------------------------------------------------------------------------

bool DragAndDrop::eventFilter(QObject *obj, QEvent *event) {
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        qDebug("Ate key press %d", keyEvent->key());
        return true;
    } else {
        // standard event processing
        return QObject::eventFilter(obj, event);
    }
}

//===============================================================================================
