#ifndef DRAGANDDROP_HPP
#define DRAGANDDROP_HPP
#pragma once
#include <QDrag>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QFrame>
#include <QLabel>
#include <QMimeData>
#include <QMouseEvent>
#include <QPainter>
#include <QPixmap>

class DragAndDrop : public QFrame {
    Q_OBJECT
   public:
    explicit DragAndDrop(QWidget *parent = nullptr);

   protected:
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    bool eventFilter(QObject *obj, QEvent *event);

   signals:
    void PosX(int);
    void PosY(int);
};

#endif  // DRAGANDDROP_HPP
