#ifndef MYDESIGNER_HPP
#define MYDESIGNER_HPP
#pragma once
#include <QMainWindow>

namespace Ui {
class MyDesigner;
}

class MyDesigner : public QMainWindow {
    Q_OBJECT

   public:
    explicit MyDesigner(QWidget *parent = Q_NULLPTR);
    ~MyDesigner();
   private slots:
    // Menu
    void on_actionAbout_triggered();
    void on_actionExit_triggered();

    // Autorun Button
    void on_tBtnAR_normal_clicked();
    void on_tBtnAR_select_clicked();
    void on_tBtnAR_click_clicked();
    void on_tBtnAR_disable_clicked();
    // Installer Button
    void on_tBtn_Normal_clicked();
    void on_tBtn_select_clicked();
    void on_tBtn_click_clicked();
    void on_tBtn_disable_clicked();

    void on_tBtnSM_normal_clicked();
    void on_tBtnSM_select_clicked();
    void on_tBtnSM_click_clicked();
    void on_tBtnSM_disable_clicked();

    void on_action_Save_Project_triggered();
    void on_actionInfo_triggered();
    void on_actionNew_Project_triggered();
    void on_action_Open_Project_triggered();

   private:
    void            CustomLayout();
    void            setImages();
    void            connections();
    Ui::MyDesigner *ui;
    class Autorun*       m_ar;
    class Installer*     m_in;
    class SmallInstaller* m_si;
};

#endif  // MYDESIGNER_HPP
