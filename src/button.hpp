#ifndef BUTTON_HPP
#define BUTTON_HPP

#pragma once

#include <QPixmap>
#include "draganddrop.hpp"

namespace {
#define GETTER(_TYPE, _FUNCTION, _VAR) \
    _TYPE _FUNCTION() {                \
        return _VAR;                   \
    }
#define SETTER(_TYPE, _FUNCTION, _VAR)  \
    void _FUNCTION(const _TYPE &_new) { \
        _VAR = _new;                    \
    }

#define PROPERTY(_TYPE, _FUNCTION, _VAR) \
    GETTER(_TYPE, _FUNCTION, _VAR)       \
    SETTER(_TYPE, _FUNCTION, _VAR)
}  // namespace

class Button : public DragAndDrop {
    Q_OBJECT

   public:
    Button(QWidget *parent = nullptr);
    PROPERTY(QPixmap, ButtonPNG, _button)
   private:
    QPixmap _button;
};

#endif  // BUTTON_HPP
