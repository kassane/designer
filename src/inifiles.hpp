#ifndef INIFILES_HPP
#define INIFILES_HPP

#include <QObject>
#include <QSettings>

class IniFiles : public QObject
{
    Q_OBJECT
public:
    explicit IniFiles(QObject *parent = nullptr);
    ~IniFiles();
    void Load();
    void Save();
    void Edit(const QString &group, const QString &value, const QString &category);
    void setfilename(const QString& filename);
    void setgroup(const QString& gname);
    void setCategory(const QString& catName);
    QString filename() const;
private:
    QString m_group="", m_value="", m_sSettingsFile="", m_pEdit="", m_filename="Setup.ini";
    QString groupName() const;
    QStringList Myvalues;
};

#endif // INIFILES_HPP
